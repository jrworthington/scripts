#!/bin/bash

HOST=192.168.1.8
SHARE=jerald
CREDENTIALS=/home/$USER/.creds
MNTPATH=~/nas

sudo mount -t cifs //$HOST/$SHARE -o credentials=$CREDENTIALS,uid=$UID $MNTPATH
