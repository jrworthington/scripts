#!/bin/bash

# This script is written to monitor the given IP address and port number
# and display only the given connections on the given port and IP, using either ss or netstat.
# Using the sed command along with the sort option, this will display only the IP addresses
# that have established connections to your device on the given local IP and port



ARGS=( "$@" )
ARG_COUNT=$#
LISTENER="127.0.0.1"
PORT=""
COMMAND=""
TIME=1
SED_STR="'s/[0-9]* *[0-9]* *$LISTENER:$PORT *//;s/:.*//'"


function main() {
  sanity
  parse
  

  watch -n $TIME "$COMMAND -nat state established | grep $LISTENER:$PORT | sed "$SED_STR" | sort -u"
}

function usage_exit() {
  echo "usage: $0 {listening IP} {listening port} {time}"
  exit -1
}

function sanity() {
  if [[ $ARG_COUNT -lt 3 ]]; then
    usage_exit
  fi

  if [ $(which ss) ]; then
    export COMMAND=ss
  elif [ $(which netstat) ]; then
    export COMMAND=netstat
  else
    echo "netstat or ss commands not installed."
    echo "Please install one of these on your system to proceed."
    exit -2
  fi
}

function parse() {
  export LISTENER="${ARGS[0]}"
  export PORT="${ARGS[1]}"
  export TIME="${ARGS[2]}"
  export SED_STR="'s/[0-9]* *[0-9]* *$LISTENER:$PORT *//;s/:.*//'"
}

main
