#!/bin/bash

trs='transmission-remote localhost:9091 -n transmission:transmission'

flavors=( 'xfce' 'kde' 'gnome' ) # flavors list

tdir='/var/lib/transmission-daemon/downloads' # torrents dir

# below block gets current torrent URL from Manjaro site for all flavors

for flavor in "${flavors[@]}"; do 
    torrver=$(curl https://manjaro.org/downloads/official/$flavor/ | egrep 'https.*\.torrent' | sed 's/^.*<//;s/a href="//;s/".*//;s/.*onClick.*//;s/.*minimal.*//;/^$/d')
    locver=$(ls $tdir | grep $flavor)
    if [[ -z $locver ]]; then
      echo torrent $flavor not on, downloading
      $trs -a $torrver
    else
      tnum=$($trs -l | grep $locver | sed 's/^ *//;s/ .*//')
      if ! $(grep -q "$locver" <<< "$torrver") ; then # check versions
        $trs -t $tnum -rad #purge
        $trs -a $torrver
      fi
    fi
done
