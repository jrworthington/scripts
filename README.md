## About this repository
---

This repository is going to house various scripts created for various GNU/Linux systems administration tasks written by me. I plan to use this repository to make obtaining scripts I may commonly need easier across a wide variety of platforms, whether it's on a desktop computer/workstation or on a physical/virtual server.

## TODO:
---
  1. Add more information in to the conmon.sh script (such as an information part [as listed in the file's internal TODO])
  2. Add description files for each script in here as well, whether adding on to this "README.md" file or adding individualized informative .md files for each script that would further detail it
  3. Recreate scripts in Python as well to make them more advanced and have better checks
