#!/usr/bin/env python3

import os
from subprocess import run, DEVNULL
from getpass import getpass
from stat import S_IREAD, S_IWUSR
from pathlib import Path

""" below constants should not be touched/modified """
STATVAL: int = S_IREAD | S_IWUSR
MODECHECK: oct = oct(0o600)
NEWDIRPERMS: oct = 0o775
SEVENS: oct = 0o777
UID = os.getuid()

""" below constants can/should be changed as needed """
CREDENTIALS: str = str(Path.home()) + "/.creds"
SHAREIP: str = "192.168.1.8"
SHARE: str = "jerald"
SHAREMNT: str = str(Path.home()) + "/nas"

def check_admin() -> bool:
    """
    Check for if current user has access to sudo/root permissions to use mount
    command
    """
    ret: bool = True
    if os.geteuid() != 0:
        msg: str = "sudo check for %u: "
        ret = not bool(run("sudo -v -p '%s'" % msg, shell=True, stdout=DEVNULL, stderr=DEVNULL).returncode)
    return ret

def check_creds_file() -> bool:
    """
    Check if $HOME/.creds file exists and has proper security permissions
    of 0o600 set, fixes security permissions if not
    """
    res: bool = False
    if os.path.exists(CREDENTIALS):
        res = True
        CREDMODE: oct = oct(os.stat(CREDENTIALS).st_mode & SEVENS)
        if CREDMODE != MODECHECK:
            print(CREDENTIALS + " not set to rw-------, fixing")
            os.chmod(CREDENTIALS, STATVAL)

    return res

def check_mountpoint_exists() -> bool:
    """
    Checks if mountpoint defined in SHAREMNT exists
    """
    status: bool = False
    if os.path.exists(SHAREMNT):
        status = True
    return status

def create_mountpoint() -> bool:
    """
    Create mountpoint if it doesn't exist
    """
    status: bool = check_mountpoint_exists()
    if not status:
        os.mkdir(SHAREMNT, NEWDIRPERMS)
        status = check_mountpoint_exists()
    return status


def create_creds_file() -> bool:
    """
    Creates $HOME/.creds file and sets appropriate permissions
    """
    status: bool=check_creds_file()
    if status:
        return status
    userstring: str = "user="
    pwstring: str = "password="

    userstring += input("Share username: ")
    pwstring += getpass("Share password: ")

    f = open(CREDENTIALS, "w")
    f.write(userstring+"\n"+pwstring+"\n")
    f.close()

    os.chmod(CREDENTIALS, STATVAL)
    CREDMODE: oct = oct(os.stat(CREDENTIALS).st_mode & SEVENS)
    if CREDMODE == MODECHECK:
        status = True

    return status

def main() -> bool:
    """ mounts nas if all checks pass """
    status: bool = False

    """ checks if admin permissions exist """
    status = check_admin()
    if not status:
        print("You do not have access to run commands as root.")
        print("Contact your systems administrator if this is a mistake.")
        print("Or close out of your shell and try again if you mistyped your\npassword too many times.")
        return False

    """ checks if credentials file exists, creates if not and user agrees """
    status = check_creds_file()
    if not status:
        print("File " + CREDENTIALS + " does not exist.")
        choice: str = input("Would you like to create it? y or n: ")
        while choice[0].lower() != 'y' and choice[0].lower() != 'n':
            print("Please enter y or n")
            choice = input("Would you like to create it? y or n: ")
        if choice[0].lower() == 'y':
            status = create_creds_file()
        else:
            print("You chose to not create a credentials file. Exiting.")
            return status

    """ checks if mountpoint exists, creates if not and user agrees """
    status = check_mountpoint_exists()
    if not status:
        print("File " + SHAREMNT + " does not exist.")
        choice: str = input("Would you like to create it? y or n: ")
        while choice[0].lower() != 'y' and choice[0].lower() != 'n':
            print("Please enter y or n")
            choice = input("Would you like to create it? y or n: ")
        if choice[0].lower() == 'y':
            status = create_mountpoint()
        else:
            print("You chose to not create the mountpoint. Exiting.")
            return status

    """ mount after all other stuff passes """
    run("sudo mount -t cifs //"+SHAREIP+"/"+SHARE+ " -o credentials="+CREDENTIALS+",uid="+str(UID)+" "+SHAREMNT, shell=True)
    #sudo mount -t cifs //$HOST/$SHARE -o credentials=$CREDENTIALS,uid=$UID $MNTPATH
    return True

main()
