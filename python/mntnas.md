# About `mntnas.py`
---

`mntnas.py` is a Python3 adaptation of the `mntnas.sh` script in the main directory of my `scripts` repository. This has a lot more sanity (read: any) sanity checks compared to the `mntnas.sh` adaptation, which was a quick-and-dirty implementation.

The existence of this script/Python file is for admins that may need to hot mount a NAS share that isn't necessarily needed on boot. I decided for this route as a project so that:

1. I could see if I could get it to work in general from a script
2. Originally this was designed on a laptop to mount a share I may or may not have an established VPN connection for to not have to have latency on boot of it attempting to be mounted

---
## Dependencies
---

There are some basic dependencies for this project:

1. You must have `sudo` access to be able to run this script
2. If running on a Debian/*buntu variant, you need `cifs-utils` to be able to mount the share as defined since it uses a CIFS mount
  - I'm sure the above applies to non Debian/*buntu variants, but I haven't really tested it on anything other than those two

  ---
## TODO:
  ---
  - [x] Write this document
  - [ ] Extend script for variable assignment from command line args
